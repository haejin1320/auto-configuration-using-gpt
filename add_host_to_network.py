from mininet.net import Mininet
from mininet.cli import CLI
from mininet.topo import Topo
from mininet.node import Host
from mininet.node import OVSKernelSwitch
from mininet.log import setLogLevel, info
from mininet.node import RemoteController
from mininet.term import makeTerm
from create_basic_topology import createBasicTopology
setLogLevel('info')

class AddHost(Topo):
    def __init__(self, previous_topology, new_connection, new_host_ip = None):
        super(AddHost, self).__init__()
        self.router_list = previous_topology.router_list

        self.host_list = previous_topology.host_list
        self.host_list.append(f'h{len(previous_topology.host_list)+1}')
        self.connection = previous_topology.connection
        new_host_name = f'h{len(self.host_list)}'

        for router in new_connection:
            self.connection[router].append(new_host_name)

        if new_host_ip is None:
            new_host_ip = f'10.0.1.{len(self.host_list)+1}/24'
        
        self.host_ip_list = previous_topology.host_ip_list.copy()
        self.host_ip_list.append(new_host_ip)

        for i in range(len(self.router_list)):
            self.addSwitch(f's{i+1}', cls = OVSKernelSwitch, failMode = 'standalone')
        

        for i in range(len(self.host_list)):
            self.addHost(f'h{i+1}', ip = self.host_ip_list[i], defaultRoute = f'via 10.0.1.254')

        for router in self.connection.keys():
            for host in self.connection[router]:
                self.addLink(router, host)

def test_add_host_to_network():
    num_host = 3
    num_router = 1
    connection = {'s1' : ['h1', 'h2', 'h3']}
    custom_topo = createBasicTopology(num_host, num_router, connection)
    new_topo = AddHost(custom_topo, ['s1'])
    net = Mininet(topo=new_topo, controller = RemoteController, ipBase='10.0.1.0/24')
    net.start()
    CLI(net)
    net.stop()


if __name__ == '__main__':
    pass