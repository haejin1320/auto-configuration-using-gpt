import openai
from mapping_examples import *

openai.api_key = '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'


def classify_intent(user_input):
    """
    Classify the network configuration intention of a natural language input.
    """
    print("Classifying network intention...")
    prompt = "I am an AI trained to classify network-related tasks. Below are examples of natural language inputs and their corresponding network configuration intentions:\n\n"
    for intent in utter_to_intent:
        prompt += f"Intent: {intent['intent']}\n"
        for example in intent['examples']:
            prompt += f"- {example}\n"
        prompt += "\n"
    prompt += f"Given the new natural language input, classify the network configuration intention. Just print the name of the name of the option.\nOption:"
    for intent in utter_to_intent:
        prompt += f"{intent['intent']}, "
    prompt += "None"
    # prompt += f"Input: {user_input}\n"
    # print("Prompt\n" + prompt)

    try:
        response = openai.chat.completions.create(
            model="gpt-3.5-turbo",
            messages = [{"role" : "system", "content" : prompt},
                        {"role" : "user", "content" : user_input}],
            # prompt=prompt,
            temperature=0.1,
            max_tokens=2048
        )
        return response.choices[0].message.content.strip()
    except openai.APIConnectionError as e:
        print(f"An error occurred: {e}")


# # Example usage
# new_input = "Introduce a new workstation identified as 'host2' into our system and establish its connection to switch 's3'."
# classification = classify_intent(new_input)
# print(f"{classification}")
