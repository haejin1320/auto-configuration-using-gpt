import openai, json
from mapping_examples import *

openai.api_key = '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'


def extract_network_params(user_input, intent):
    """
    Extracts network configuration parameters from a natural language command using OpenAI's GPT-3.5-turbo-instruct model.
    """
    print("Extracting network parameters...")
    prompt = f"I am an AI trained to classify network-related tasks. I will extract parameters from natural language descriptions of network configurations. The user input is about {intent}. Below are examples of natural language descriptions and their corresponding network configuration parameters. The output should be json format. Do not print anything except the output with json format:\n\n"
    for i in range(len(utter_to_params[intent]['descriptions'])):
        prompt += f"Description: {utter_to_params[intent]['descriptions'][i]}\n"
        param_dict = {}
        for item in utter_to_params[intent]['parameters'][i]:
            key, value = item.split(':', 1)
            key = key.strip()
            value = value.strip()
            if value == "None":
                value = None
            else:
                value = str(value)
                param_dict[key] = value
        prompt += json.dumps(param_dict, indent=2)
        prompt += "\n\n"
    # prompt += f"Description: {user_input}"
    # print(prompt)
    try:
        response = openai.chat.completions.create(
            model="gpt-3.5-turbo",
            messages = [{"role" : "system", "content" : prompt},
                        {"role" : "user", "content" : user_input}],
            # prompt=prompt,
            temperature=0.1,
            max_tokens=2048
        )
        return response.choices[0].message.content.strip()
    except openai.APIConnectionError as e:
        print(f"An error occurred: {e}")

# # Example usage
# new_input = "Introduce a new workstation identified as 'host2' into our system and establish its connection to switch 's3'."
# response = extract_network_parmas(new_input, "Add a host to the network")

# print(f"{response}")
