
from mininet.net import Mininet
from mininet.cli import CLI
from mininet.topo import Topo
from mininet.node import Host
from mininet.node import OVSKernelSwitch
from mininet.log import setLogLevel, info
from mininet.node import RemoteController
from mininet.term import makeTerm
setLogLevel('info')



class createBasicTopology(Topo):
    def __init__(self, num_host, num_router, connection, host_ip_list = None):
        super(createBasicTopology, self).__init__()
        self.router_list = []
        self.host_list = []
        self.connection = connection
        
        default_host_ip_list = [f'10.0.1.{i+2}/24' for i in range(num_host)]
        self.host_ip_list = host_ip_list if host_ip_list is not None else default_host_ip_list

        for i in range(num_router):
            router = self.addSwitch(f's{i+1}', cls = OVSKernelSwitch, failMode = 'standalone')
            self.router_list.append(router)
        

        for i in range(num_host):
            if host_ip_list is None:
                host = self.addHost(f'h{i+1}', ip = default_host_ip_list[i], defaultRoute = f'via 10.0.1.254')
                self.host_list.append(host)
            else:
                host = self.addHost(f'h{i+1}', ip = host_ip_list[i], defaultRoute = f'via 10.0.1.254')
                self.host_list.append(host)

        for router in connection.keys():
            for host in connection[router]:
                self.addLink(router, host)

def test_create_basic_topology():
    num_host = 3
    num_router = 1
    connection = {'s1' : ['h1', 'h2', 'h3']}

    custom_topo = createBasicTopology(num_host, num_router, connection)
    net = Mininet(topo=custom_topo, controller = RemoteController, ipBase='10.0.1.0/24')
    net.start()
    CLI(net)
    net.stop()


if __name__ == '__main__':
    pass