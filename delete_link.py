
from mininet.net import Mininet
from mininet.cli import CLI
from mininet.topo import Topo
from mininet.node import Host
from mininet.node import OVSKernelSwitch
from mininet.log import setLogLevel, info
from mininet.node import RemoteController
from mininet.term import makeTerm
from create_basic_topology import createBasicTopology
setLogLevel('info')

class DeleteLink(Topo):
    def __init__(self, previous_topology, delete_link):
        super(DeleteLink, self).__init__()
        self.router_list = previous_topology.router_list
        self.host_list = previous_topology.host_list
        self.connection = previous_topology.connection
        self.host_ip_list = previous_topology.host_ip_list

        (delete_host, delete_router) = delete_link
        self.connection[delete_router].remove(delete_host)

        for i in range(len(self.router_list)):
            self.addSwitch(f's{i+1}', cls = OVSKernelSwitch, failMode = 'standalone')

        for i in range(len(self.host_list)):
            self.addHost(f'h{i+1}', ip = self.host_ip_list[i], defaultRoute = f'via 10.0.1.254')

        for router in self.connection.keys():
            for host in self.connection[router]:
                self.addLink(router, host)

def test_delete_link():
    num_host = 3
    num_router = 1
    connection = {'s1' : ['h1', 'h2', 'h3']}
    custom_topo = createBasicTopology(num_host, num_router, connection)
    new_topo = DeleteLink(custom_topo, ('h1', 's1'))
    net = Mininet(topo=new_topo, controller = RemoteController, ipBase='10.0.1.0/24')
    net.start()
    CLI(net)
    net.stop()



if __name__ == '__main__':
    test_delete_link()