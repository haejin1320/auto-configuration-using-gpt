import json, openai

def input() -> (str, dict):
    user_input = input("User input : ")
    intent = classify_intent(user_input)
    print("Intent : " + intent)
    params = extract_network_params(user_input, intent)
    return intent, json.loads(params)