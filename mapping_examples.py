
openai_api_key = ''

utter_to_intent = [
    {
        "intent": "Add a host to the network",
        "examples": [
            "Include another PC in our network.",
            "Attach an extra host to our topology."
        ]
    },
    {
        "intent": "Disconnect a link",
        "examples": [
            "Cut the connection between two nodes.",
            "Sever the network tie from this node."
        ]
    },
    {
        "intent": "Create a basic network topology",
        "examples": [
            "Initiate a network with one switch, two hosts.",
            "Formulate basic network with nodes and a switch."
        ]
    },
    {
        "intent": "Configure OSPF on a router",
        "examples": [
            "Enable OSPF protocol on our main router.",
            "Set up route calculation using OSPF on our router."
        ]
    },
    {
        "intent": "Configure router interfaces",
        "examples": [
            "Update router interfaces with new IPs.",
            "Adjust router interface for subnet connection."
        ]
    },
    {
        "intent": "Configure static routing",
        "examples": [
            "Create a permanent route for data center traffic.",
            "Insert a static route in the router for the backup site."
        ]
    },
    {
        "intent": "Secure the network with a firewall",
        "examples": [
            "Steps to add a firewall for network defense?",
            "Guide on setting up network firewall?"
        ]
    }
]

utter_to_params = {
    "Add a host to the network": {
        "descriptions": [
            "Add a new computer to the network, it should be named 'host1' and connected to switch 's1'.",
            "Include another PC with IP 192.168.1.10 in our network and attach it to 's2'."
        ],
        "parameters": [
            ["Host Name: host1", "Host IP Address: None", "Switch: s1"],
            ["Host Name: None", "Host IP Address: 192.168.1.10", "Switch: s2"]
        ]
    },
    "Disconnect a link": {
        "descriptions": [
            "Cut the connection between two nodes named 'node1' and 'node2'.",
            "Sever the network tie from 'node1' to 'node2'."
        ],
        "parameters": [
            ["Node 1 Name: node1", "Node 2 Name: node2"],
            ["Node 1 Name: node1", "Node 2 Name: node2"]
        ]
    },
    "Create a basic network topology": {
        "descriptions": [
            "Initiate a network with one switch, two hosts.",
            "Construct a simple topology with a single switch 's2' and three hosts, 'h3', 'h4', and 'h5', each connected to 's2'."
        ],
        "parameters": [
            ["Number of Switches: 1", "Number of Hosts: 2",
             "Host Names: None", "Switch Names: None", "Connections: None"],
            ["Number of Switches: 1", "Number of Hosts: 3", "Host Names: h3, h4, h5",
             "Switch Names: s2", "Connections: h3-s2, h4-s2, h5-s2"]
        ]
    },
    "Configure OSPF on a router": {
        "descriptions": [
            "Activate OSPF on the router 'R1', enable it on interfaces 'eth0' and 'eth1', designate it as part of area '0', and set OSPF priority to '100' on 'eth0'.",
            "Configure OSPF routing on router 'R2' with router ID '2.2.2.2', apply it to interfaces 'eth2' and 'eth3', assign area '0.0.0.0', and adjust the hello interval to '30' seconds."
        ],
        "parameters": [
            ["Router Name/ID: R1", "Interfaces: eth0, eth1", "Area Number: 0",
             "Additional OSPF Settings: OSPF priority 100 on eth0"],
            ["Router Name/ID: R2", "Interfaces: eth2, eth3", "Area Number: 0.0.0.0",
             "Additional OSPF Settings: Hello interval 30 seconds"]
        ]
    },
    "Configure router interfaces": {
        "descriptions": [
            "Assign IP '192.168.1.1/24' to interface 'eth0' on router 'R1' and IP '192.168.2.1/24' to interface 'eth1'.",
            "Set up interface 'eth2' on router 'R2' for subnet '10.0.0.0/16' with the IP '10.0.0.1/16'."
        ],
        "parameters": [
            ["Router Name/ID: R1", "Interface eth0: 192.168.1.1/24",
             "Interface eth1: 192.168.2.1/24"],
            ["Router Name/ID: R2", "Interface eth2: 10.0.0.1/16", "Subnet: 10.0.0.0/16"]
        ]
    },
    "Configure static routing": {
        "descriptions": [
            "Set a static route on router 'R1' for the data center subnet '172.16.0.0/24' via gateway '192.168.1.254'.",
            "Add a static route to router 'R2' for the backup site subnet '10.0.2.0/24' through next-hop '192.168.2.254'."
        ],
        "parameters": [
            ["Router: R1", "Destination Subnet: 172.16.0.0/24",
             "Gateway: 192.168.1.254"],
            ["Router: R2", "Destination Subnet: 10.0.2.0/24",
             "Gateway: 192.168.2.254"]
        ]
    },
    "Secure the network with a firewall": {
        "descriptions": [
            "On 'Router1', establish a firewall rule to permit inbound traffic on ports 80 and 443, and block all other ports.",
            "On 'Firewall1', create an outbound rule to allow traffic to the '192.168.1.0/24' subnet, and block all other outbound traffic."
        ],
        "parameters": [
            ["Device: Router1", "Rule Type: Inbound", "Allowed Ports: 80, 443",
             "Action: Allow", "Default Action: Block"],
            ["Device: Firewall1", "Rule Type: Outbound",
             "Allowed Destination Subnet: 192.168.1.0/24", "Action: Allow", "Default Action: Block"]
        ]
    }
}
